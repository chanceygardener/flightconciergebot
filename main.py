#!/usr/bin/python3

from flask import Flask, request
from flight_data_client import FlightAwareClient
import os
import requests
import json
from fulfillment import FulfillmentEngine

app = Flask("FlightWatch")
fl = FulfillmentEngine()

# Begin helper Functions


def get_discourse_state_data(test=False):
    '''return a tuple of the form:
     (intent(string), params(dict))'''
    request_dat = request.get_json(silent=True)
    intent = request_dat['queryResult']['intent']['displayName']
    params = request_dat['queryResult']['parameters']
    session = request_dat['session']
    if test:
        params['user_query'] = request_dat['queryResult']['queryText']
    # Prior contexts are part of the previous
    # complete query where
    # remembered contexts are from incomplete queries.
    # TODO: refactor for clarity in variable names re: this
    if len(request_dat['queryResult']['outputContexts']) > 1:
        sysct = "{}/contexts/__system_counters__".format(
            session)
        passed_contexts = (c for c in request_dat['queryResult']['outputContexts']
                           if c['name'] != sysct)
        for c in passed_contexts:
            context_params = c['parameters']
            remembered = {k[11:]: context_params[k]
                          for k in context_params if k.startswith('remembered_')}
            prior = {k: context_params[k]
                     for k in context_params if k.startswith('prior_')}
            params.update(remembered)
            params.update(prior)
    else:
        # add empty string as value for 'prior' params
        # to allow conditioning on whether or not these
        # params are set
        params.update({
            "prior_airline": "",
            "prior_flightno": ""
            })
    return (intent, params, session)

# End Helper Functions

# Intent Map


intent_map = {
    'IsFlightOnSchedule': fl.IsFlightOnSchedule,
    'HasFlightArrived': fl.HasFlightArrived,
    'AirportsInCity': fl.AirportsInCity,
    'NeedMoreFlightInfo': fl.NeedMoreFlightInfo,
    'ProvideTestFlight': fl.ProvideTestFlight,
    'FlightCheckpointTime': fl.FlightTimeDepartureOrArrival,
    'FlightActionPunt': fl.FlightActionPunt
}

# End Intent Map


@app.route('/test', methods=['POST'])
def test():
    print(request.json)
    return '', 200


@app.route('/')
def index():
    pass


@app.route('/fulfillment', methods=['POST'])
def fulfillment():
    intent, params, session = get_discourse_state_data()
    nlg_func = lambda p, s: intent_map[intent](fl, p, s)
    agent_response = nlg_func(params, session)
    return json.dumps(agent_response), 200


@app.route('/fulfillment_test', methods=['POST'])
def fulfillment_test():
    intent, params, session = get_discourse_state_data(test=True)
    nlg_func = intent_map[intent]
    agent_response = nlg_func(params, session)
    print("\n\nUser: {}\n\nAgent: {}\n\n".format(params['user_query'], agent_response['fulfillmentText']))
    print(params)
    print("_"*50)
    return json.dumps(agent_response), 200

    # run Flask app
if __name__ == "__main__":
    app.run()

# curl - H "Content-Type: application/json; charset=utf-8" - H "Authorization: Bearer ya29.c.Kl60BzxNgkio51QTUVkqiY0EgH_U6lfTkLEXwNdVpWRByYzev92xhf935BYTIbFxlJuyRaVNbG4fch2fO0Tsz8uFd50Nm1LE_LVvcYF2YISc5QStmZd0jy0gPdPuBtG2" - d "{\"queryInput\":{\"text\":{\"text\":\"when's my united flight\",\"languageCode\":\"en\"}},\"queryParams\":{\"timeZone\":\"America/Los_Angeles\"}}" "https://dialogflow.googleapis.com/v2/projects/flightconcierge-ovocpb/agent/sessions/bb1883ab-d419-fe9f-573d-159b5469fad9:detectIntent"
