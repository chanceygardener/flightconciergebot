#!/usr/bin/python3

import re
import json
from datetime import datetime
from datetime import timedelta

# Utility functions

# 2019-12-12T12:00:00-08:00

# load airport data to suggest
# airports if only a city is
# given
with open('data/airports.json') as dfile:
    AIRPORT_DATA = json.loads(dfile.read())
with open('data/airline_codes.json') as dfile:
    AIRLINE_CODES = json.loads(dfile.read())

isopat = r'(?P<year>\d{4})-(?P<month>\d{2})-(?P<day>\d{2})T(?P<hour>\d{2}):(?P<minute>\d{2}):(?P<second>\d{2})(?P<shiftop>[+-])(?P<shr>\d{2}):(?P<smin>\d{2})'
isomatch = re.compile(isopat)

ord_suffixes = {
    '1': "st",
    '2': "nd",
    '3': "rd"
}


def ordsuffix(num):
    nstr = str(num)[-1]
    try:
        return ord_suffixes[nstr]
    except KeyError:
        return 'th'


def isoToDateTime(isostring):
    # GCP's python interpreter doesn't
    # support datetime.fromisoformat,
    # so here's a workaround function
    match = isomatch.match(isostring)
    if not match:
        raise ValueError("Unrecognized format: {}".format(isostring))
    groups = match.groupdict()
    dt_obj = datetime(int(groups['year']), int(groups['month']),
                      int(groups['day']), int(groups['hour']),
                      int(groups['minute']))
    if groups['shiftop'] == '+':
        dt_obj += timedelta(hours=int(groups['shr']))
        dt_obj += timedelta(minutes=int(groups['smin']))
    else:
        dt_obj -= timedelta(hours=int(groups['shr']))
        dt_obj -= timedelta(minutes=int(groups['smin']))
    return dt_obj


def iso8601ToUnixDate(datestring):
    return int(isoToDateTime(datestring).timestamp())


def nl_list(l, conjunction="and"):
    return ", ".join(l[:-1])+" "+conjunction+" "+l[-1]


def nearest_now(datetimes, no_past=False, no_future=False):
    now = datetime.now().timestamp()
    return min(sorted(dt for dt in datetimes if dt > now))


def timeDeltaFromTimeStampDiff(diff):
    '''given a difference in seconds,
    return a timedelta object with
    time units appropriately allocated'''
    days, sec_remainder = divmod(diff, 86400)
    hours, sec_remainder = divmod(sec_remainder, 3600)
    minutes, seconds = divmod(sec_remainder, 60)
    return timedelta(days=days, hours=hours,
                     minutes=minutes, seconds=seconds)


def parseFlightCode(flight_code,
                    matcher=re.compile(r'([A-Z]+)(\d+)')):
	match = matcher.match(flight_code)
	return (match.group(1), match.group(2))
