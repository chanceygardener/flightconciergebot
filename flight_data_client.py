#!/usr/bin/python3

from requests import get
from requests.auth import HTTPBasicAuth
from datetime import datetime
import json
import os


class FlightAwareApiError(Exception):
	pass


class FlightNotFoundError(Exception):
	pass


class ScheduleNotFoundError(Exception):
	pass


class FlightAwareClient:

	endpoint = "https://flightxml.flightaware.com/json/FlightXML2/"

	def __init__(self, username, api_key):
		self.auth = HTTPBasicAuth(username, api_key)

	def _errCheckResponse(self, response, query, fail_on_fa_error=True):
		rdat = json.loads(response.text)
		if response.status_code > 299:
			if response.status_code > 399:
				err_desc = "Bad query: {}\n".format(query)
			else:
				err_desc = "Internal server error: "
			raise LookupError(
				"{}Request returned code: {}. Reason: {}\n{}".format(err_desc,
																	 response.status_code,
																	 response.reason, query))
		if fail_on_fa_error:
			if 'error' in rdat.keys():
				raise FlightAwareApiError(
					"Bad flightaware api call: {}".format(rdat['error']))
		return rdat

	def _apicall(self, method, params=None, kv_formatter='{}={}', dbg=False):
		'''make generic api call'''
		params = dict() if params is None else params
		query = self.endpoint + method + "?"
		# add args
		query += "&".join((kv_formatter.format(k, v)
						   for (k, v) in params.items()))
		response = get(query, auth=self.auth)
		if dbg:
			print("flightaware api query:\n"+query)
		rdat = self._errCheckResponse(response, query)
		return rdat

	def _apisearch(self, query, how_many=100, fail_on_fa_error=True):
		query = self.endpoint + "SearchBirdseyeInFlight" + \
			"?query={}&howMany={}".format(query, how_many)
		print(query)
		response = get(query, auth=self.auth)
		rdat = self._errCheckResponse(
			response, query, fail_on_fa_error=fail_on_fa_error)
		if 'error' not in rdat.keys():
			return rdat['SearchBirdseyeInFlightResult']['aircraft']
		else:
			return rdat

	def _getUrlQuery(self, q):
		query = self.endpoint + "SearchBirdseyeInFlight" + \
			"?query={}".format(q)
		return query

	# "Public" (though not really public because python) methods.
	def getScheduledFlights(self, airport, startDate, endDate):
		result = self._apicall(
			'AirlineFlightSchedules', params={'airport': airport, 'startDate': startDate, 'endDate': endDate})
		if not result["AirlineFlightSchedulesResult"]["data"]:
			raise ScheduleNotFoundError
		else:
			return result

	def byId(self, flight_code):
		return self._apisearch("{{ident_or_reg%20{{{}}}%20}}".format(flight_code))

	def byAirline(self, airline):
		return self._apisearch("{{ident_or_reg%20{{{}*}}%20}}".format(airline))

	def findFlightNumber(self, airline, origin, destination):
		res = self._apisearch(
			"{{ident_or_reg%20{{{}*}}%20{{=%20orig%20{}}}%20{{=%20dest%20{}}}%20}}".format(airline,
																						   origin, destination))
		return res

	def findFlight(self, flightno, airline):
		result = self._apisearch(
			"{{ident_or_reg%20{{{}{}}}%20}}".format(airline, flightno), fail_on_fa_error=False, how_many=1)
		if isinstance(result, dict) and result['error'].startswith('NO_DATA'):
			print(result['error'])
			raise FlightNotFoundError(airline+str(flightno))
		return result[0] if result else None

	def getDelay(self, airline, flightno, dbg=False, HALF_WINDOW=43200):
		flight_struct = self.findFlight(flightno, airline)
		# Give 12 hour window from departure time for search bounds
		# This does fail on the edge-case of a delay over 12 hours
		if flight_struct['departureTime']:
			 search_start = int(flight_struct['departureTime'] - HALF_WINDOW)
			 search_end = int(flight_struct['departureTime'] +HALF_WINDOW)
		else:
			now = datetime.now().timestamp()
			search_start = int(now - HALF_WINDOW)
			search_end = int(now + HALF_WINDOW)
		try:
			sched = self.searchScheduledFlights(startDate=search_start,
											endDate=search_end, flightno=flightno, airline_code=airline,
											origin=flight_struct['origin'], destination=flight_struct['destination'], dbg=dbg)[-1]
		except IndexError:
			raise ScheduleNotFoundError
		if flight_struct['arrivalTime']:
			schdiff_arr = int(
				flight_struct['arrivalTime'] - sched['arrivaltime'])
		else:  # assign None to schedule difference, indicating that the flight has not
			# arrived yet, as opposed to 0, which indicates the unlikely event
			# of a flight being exactly on time.
			schdiff_arr = None
		if dbg:
			print(sched)
			print(flight_struct)
		sch_diff_struct = {
			'arrival': schdiff_arr,
			'departure': flight_struct['departureTime'] - sched['departuretime'],
			'ident': airline + str(flightno),
			'origin': flight_struct['origin'],
			'destination': flight_struct['destination'],
			'arrival_sched': sched['arrivaltime'],
			'departure_sched': sched['departuretime'],
			'arrival_act': flight_struct['arrivalTime'],
			'departure_act': flight_struct['departureTime']
		}
		return sch_diff_struct

	def searchScheduledFlights(self, startDate=int(datetime.now().timestamp()),
							   endDate=int(datetime.now().timestamp()+86400),
							   airline_code='', flightno='', origin='', destination='', dbg=False):
		locs = locals()
		params = {v: locs[v] for v in locs if locs[v]
				  != '' and v not in ('self', 'dbg')}
		scheduled_flight_dat = self._apicall(
			'AirlineFlightSchedules', params=params, dbg=dbg)
		try:
			return scheduled_flight_dat['AirlineFlightSchedulesResult']['data']
		except KeyError:
			p = ", ".join("{}: {}".format(i[0], i[1]) for i in params.items())
			raise ValueError("What's up with {}\nparams: {}".format(
				str(scheduled_flight_dat), str(p)))

	def _test(self):
		return get("https://flightxml.flightaware.com/json/FlightXML2/Search?query=-latlong%20%2236.897669%20-79.03655%2040.897669%20-75.03655%22&howMany=10&offset=0", auth=self.auth)
