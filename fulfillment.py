#!/usr/bin/python3

import json
import random
import os
from datetime import datetime
from dotenv import load_dotenv
from nlg import NlgServer
from flight_data_client import FlightAwareClient, FlightNotFoundError, ScheduleNotFoundError, FlightAwareApiError
from utils import *

# NLG logic

# Load FlightAware api client
load_dotenv('.env')
fa_uname = os.getenv('FLIGHTAWARE_UNAME')
fa_key = os.getenv('FLIGHTAWARE_KEY')


class FulfillmentEngine:

	def __init__(self):
		self.fa_client = FlightAwareClient(fa_uname, fa_key)
		self.nlg = NlgServer()


	def AirportsInCity(self, params, session):
		webhook_response = webhook_response = {
			'fulfillmentText': '',
			'payload': {
				"google": {
					"expectUserResponse": True
				}
			},
			'outputContexts': []
		}
		poss = [AIRPORT_DATA[a] for a in AIRPORT_DATA
				if params['city'].lower() == AIRPORT_DATA[a]['city'].lower()]
		if params['state']:
			poss = [a for a in poss
					if params['state'].lower() == a['state'].lower()]
		if params['country']:
			poss = [a for a in poss
					if params['country'].lower() == a['country'].lower()]
		# Get unique cities
		city_set = list(set((a['city'], a['state'], a['country']) for a in poss))
		if len(city_set) == 0:
			webhook_response['fulfillmentText'] = self.nlg.CityNotFoundResponse(
				params['city'])
		elif len(city_set) > 1:
			webhook_response['fulfillmentText'] = self.nlg.MultipleCitiesFound(
				city_set)
			webhook_response['outputContexts'].append(
				{
					'name': "{}/contexts/incomplete-flight-information".format(session),
					'lifespanCount': 1,
					'parameters': {val: params[val] for val in params if val}
				}
			)
		else:
			webhook_response['fulfillmentText'] = self.nlg.CityAirportList(
				params['city'], poss)

		return webhook_response


	def _returnInsufficient(self, resp, params, session, context_name):
		'''Update webhook response in place to reflect
		that infsufficient information has been
		provided'''
		resp['fulfillmentText'] = self.nlg.GenerateMoreInfoPrompt(
			params)
		# Pass parameters to be remembered from
		# this discourse turn to the next
		resp['outputContexts'].append(
			{
				'name': "{}/contexts/{}".format(session, context_name),
				'lifespanCount': 1,
				'parameters': {"remembered_" + val: params[val]
							   for val in params if params[val]}
			}
		)


	def NeedMoreFlightInfo(self, params, session):
		webhook_response = {
			'fulfillmentText': '',
			'payload': {
				"google": {
					"expectUserResponse": True
				}
			},
			'outputContexts': []
		}
		if params['airline'] and params['flightno']:
			return self.IsFlightOnSchedule(params, session)
		else:
			_returnInsufficient(webhook_response, params, session,
								'incomplete-flight-information')
			return webhook_response


	def IsFlightOnSchedule(self, params, session):
		webhook_response = {
			'fulfillmentText': '',
			'outputContexts': []
		}
		if not params['airline'] and not params['flightno']:
			# See comments at line 180,
			# this block basically allows
			# the NLG engine to know
			# about the last flight mentioned context
			elide = True
			if params['prior_airline']:
				al_name = params['prior_airline']
			else:
				al_name = params['airline']
			if params['prior_flightno']:
				flightno = params['prior_flightno']
			else:
				flightno = params['flightno']
		else:
			al_name, flightno = params['airline'], int(params['flightno'])
			elide = False
		if al_name and flightno:
			try:
				airline = AIRLINE_CODES[al_name.lower()]
			except KeyError:
				webhook_response['fulfillmentText'] = self.nlg.UnrecognizedAirlinePunt(
					al_name)
				return webhook_response

			# At this point, we've recognized the flightno and airline, 
			# and can safely pass it as an output context
			webhook_response['outputContexts'].append(
			{
				'name': "{}/contexts/{}".format(session, 'last_flight_mentioned'),
				'lifespanCount': 1,
				'parameters': {"prior_airline": al_name,
							   "prior_flightno": flightno}
			}
		)
			try:
				delay_or_fail = self.fa_client.getDelay(
					airline, int(flightno))
				webhook_response['fulfillmentText'] = self.nlg.FlightScheduleResponse(
					delay_or_fail, al_name, int(flightno), chckpt=params['flight_checkpoint'],
					user_schedule_focus=params['user_scheduling_focus'], elide=elide)

			except FlightNotFoundError:
				webhook_response['fulfillmentText'] = self.nlg.FlightNotFoundSmartPunt(int(flightno),
																						 al_name)
			except ScheduleNotFoundError:
				# TODO: separate logic in GetDelay, despite
				# the fact that this should not often be the case,
				# i.e., generally flights that are found successfully
				# should be found the the schedules.
				# As it stands, this will require 2 findFlight api calls
				# instead of one.
				flight_struct = self.fa_client.findFlight(
					int(flightno), airline)
				webhook_response['fulfillmentText'] = self.nlg.FoundFlightNotScheduleResponse(
					al_name,
					int(flightno),
					self.nlg._cityFromICAO(
						flight_struct['origin']),
					self.nlg._cityFromICAO(
						flight_struct['destination']),
					int(
						flight_struct['departureTime']),
					int(flight_struct['arrivalTime']))
				return webhook_response

		else:
			_returnInsufficient(webhook_response, params, session,
								'incomplete-flight-information')
		return webhook_response


	def FlightTimeDepartureOrArrival(self, params, session):
		webhook_response = {
			'fulfillmentText': '',
			'outputContexts': []
		}
		# Intent is triggered, but the user provided no 
		# 
		if not params['airline'] and not params['flightno']:
			# Assume we're talking about the last
			# complete flight mentioned
			# The conditionals may be a bit clunky,
			# but it allows us to bring
			# any prior stated parameters into
			# the current context even if only one
			# was mentioned
			elide = True
			if params['prior_airline']:
				al_name = params['prior_airline']
			else:
				al_name = params['airline']
			if params['prior_flightno']:
				flightno = params['prior_flightno']
			else:
				flightno = params['flightno']
		else:
			al_name, flightno = params['airline'], params['flightno']
			elide = False
		if al_name and flightno:
			try:
				airline = AIRLINE_CODES[al_name.lower()]
			except KeyError:
				webhook_response['fulfillmentText'] = self.nlg.UnrecognizedAirlinePunt(
					params['airline'])
				return webhook_response
			webhook_response['outputContexts'].append(
				{
					'name': "{}/contexts/{}".format(session, 'last_flight_mentioned'),
					'lifespanCount': 1,
					'parameters': {"prior_airline": al_name,
								   "prior_flightno": flightno}
				}
			)
			try:
				flight_struct = self.fa_client.findFlight(
				int(flightno), airline)
				webhook_response['fulfillmentText'] = self.nlg.FlightTimeDepOrArrResponse(
				params['checkpoint'],
				al_name,
				int(flightno),
				self.nlg._cityFromICAO(
					flight_struct['origin']),
				self.nlg._cityFromICAO(
					flight_struct['destination']),
				int(
					flight_struct['departureTime']),
				int(flight_struct['arrivalTime']),
				elide=elide)
			except FlightNotFoundError:
				webhook_response['fulfillmentText'] = self.nlg.FlightNotFoundSmartPunt(int(flightno),
																						 al_name)
			
			
		else:
			_returnInsufficient(webhook_response, params, session,
								'incomplete-flight-information')
		return webhook_response


	def FlightActionPunt(self, params, session):
		webhook_response = {
			'fulfillmentText': '',
			'outputContexts': [
			{
					'name': "{}/contexts/{}".format(session, 'last_flight_mentioned'),
					'lifespanCount': 1,
					'parameters': {"prior_airline": al_name,
								   "prior_flightno": flightno}
				}
				]
		}
		if not params['airline'] and not params['flightno']:
			# See comments at line 180,
			# this block basically allows
			# the NLG engine to know
			# about the last flight mentioned context
			# TODO: encapsulate this logic
			# to avoid redundancy
			if params['prior_airline']:
				al_name = params['prior_airline']
			else:
				al_name = params['airline']
			if params['prior_flightno']:
				flightno = params['prior_flightno']
			else:
				flightno = params['flightno']
		webhook_response['fulfillmentText'] = self.nlg.FlightActionNotUnderstood(al_name, int(flightno))
		return webhook_response


	def HasFlightArrived(self, params, session):
		webhook_response = {
			'fulfillmentText': '',
			'outputContexts': []
		}
		if not params['airline'] and not params['flightno']:
			# See comments at line 180,
			# this block basically allows
			# the NLG engine to know
			# about the last flight mentioned context
			# TODO: encapsulate this logic
			# to avoid redundancy
			elide = True
			if params['prior_airline']:
				al_name = params['prior_airline']
			else:
				al_name = params['airline']
			if params['prior_flightno']:
				flightno = params['prior_flightno']
			else:
				flightno = params['flightno']
		else:
			al_name, flightno = params['airline'], params['flightno']
			elide = False
		if al_name and flightno:
			try:
				airline = AIRLINE_CODES[al_name.lower()]
			except KeyError:
				webhook_response['fulfillmentText'] = self.nlg.UnrecognizedAirlinePunt(
					params['airline'])
				return webhook_response
			webhook_response['outputContexts'].append(
				{
					'name': "{}/contexts/{}".format(session, 'last_flight_mentioned'),
					'lifespanCount': 1,
					'parameters': {"prior_airline": al_name,
								   "prior_flightno": flightno}
				}
			)
			try:
				flight_or_fail = self.fa_client.findFlight(
					int(flightno), airline)
				webhook_response['fulfillmentText'] = self.nlg.HasFlightArrivedResponse(
					flight_or_fail)
			except FlightNotFoundError:
				webhook_response['fulfillmentText'] = self.nlg.FlightNotFoundSmartPunt(int(flightno),
																						 airline=al_name)
		else:
			_returnInsufficient(webhook_response, params, session,
								'incomplete-flight-information')
		return webhook_response


	def ProvideTestFlight(self, params, session, timeout_ct=10):
		webhook_response = {
			'fulfillmentText': '',
			'outputContexts': []
		}
		examples = None
		ctr = 0
		while not examples:
			airline = random.choice(list(set(AIRLINE_CODES.values())))
			# Filter for those that have either arrived or departed,
			# otherwise the bot will just say the flight has neither
			# arrived or departed, a less interesting test case
			try:
				examples = [f for f in self.fa_client.byAirline(airline)
						if f['departureTime'] != 0]
			except FlightAwareApiError:
				examples = []
				# Log that in case we get stuck in a loop
			finally:
				ctr += 1
				if ctr > timeout_ct:
					webhook_response['fulfillmentText'] = self.nlg.Punt()
					return webhook_response

		ex = examples[0]
		flight_code = ex['ident']
		airline_code, flightno = parseFlightCode(flight_code.strip())
		airline = self.nlg._airlineNameFromICAO(airline_code)
		webhook_response['fulfillmentText'] = self.nlg.ProvideExampleFlightResponse(
			airline, flightno)
		return webhook_response
