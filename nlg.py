#!/usr/bin/python3

import random
from datetime import datetime, timedelta
from utils import timeDeltaFromTimeStampDiff, AIRPORT_DATA, AIRLINE_CODES, ordsuffix, nl_list

# Contains NLG variant sets
# as lists of string variants
# intended to be called using
# random.choice(VariantSet)
# to produce more dynamic output


class NlgTemplate:
    def __init__(self, *variants):
        self.variants = variants

    def _realize(self, params=None):
        if params:
            out = random.choice(self.variants).format(**params)
        else:
            out = random.choice(self.variants)
        return out


Ok = NlgTemplate(
    "Alright",
    "Ok"
)

GenericPunt = NlgTemplate(
    "Sorry, I'm having some issues getting that information.",
    "Sorry, there was some issue getting that information.",
    "I'm having an issue accessing that information right now."
)

UnrecognizedAirline = NlgTemplate(
    "I don't think I've heard of an airline called {airline}, but that might just be me.. I am a demo after all.",
    "Sorry, I'm not aware of an airline called {airline}. It may well exist though, I'm still learning.",
    "I'm afraid I don't recognize {airline}. Is there another name for the airline?"
)

NoFlightsGivenParams = NlgTemplate(
    "So I've looked, but it doesn't look like there is a flight ",
    "I gave it a look, but I can't find a flight ",
    "I can't seem to find a flight ",
    "Where I'm looking doesn't seem to have a listing of any flight "
)

AirlinePp = NlgTemplate(
    "with {airline} ",
    "on {airline} "
)

OriginPp = NlgTemplate(
    "from {origin} ",
    "going from {origin} "
)

DestinationPp = NlgTemplate(
    "to {destination} ",
    "over to {destination} "
)

DepartureDateTimePp = NlgTemplate(
    "on {departure_time} ",
    "leaving on {departure_time} ",
    "departing on {departure_time} "
)

AroundAbout = NlgTemplate(
    "around",
    "about"
)

# time_diff_dep: (X minutes (early|late) | on time)

MoreInfoPrompt = NlgTemplate(
    "I'm going to need {missing_info} to get any info on your flight.",
    "You'll have to give me {missing_info} for flight updates.",
    "I'll need {missing_info} to find your flight.",
    "I need {missing_info} to know what flight you're talking about.",
    "You'll have to tell me {missing_info} for me to find the flight you're looking for.",
    "I need {missing_info} to know how to find the flight you're looking for."
)

HasNotArrived = NlgTemplate(
    "actually hasn't arrived in {destination} yet.",
    "hasn't landed in {destination} yet.",
    "actually hasn't gotten into {destination} yet.",
    "has yet to to arrive in {destination}."
)

# For Time Diffs
HasArrived = NlgTemplate(
    "landed in {destination} {time_diff_arr}",
    "touched down in {destination} {time_diff_arr}",
    "was {time_diff_arr} landing in {destination}",
    "got into {destination} {time_diff_arr}"
)

UnrecognizedAirline = NlgTemplate(
    "I actually haven't heard of {airline} before. "
    "Though that may just be me, I am still just a working demo.",
    "That's the first time I'm hearing of {airline} unfortunately, "
    "I am pretty new on the scene though, so it may just be me.",
    "{airline} is not an airline I've heard of before, sad to say."
)

ExampleFlightFrame = NlgTemplate(
    ", here's a flight: {airline} flight {flightno}.",
    ", you could try {airline} flight number {flightno}.",
    ", {airline} flight {flightno} is in my scope right now.",
    ", how about {airline} flight number {flightno}.",
    ", what about {airline} flight number {flightno}."
)

FlightHasNotArrivedFrame = NlgTemplate(
    "It doesn't look like that flight has gotten in yet.",
    "No, it hasn't arrived yet.",
    "That one hasn't arrived yet.",
    "That flight hasn't touched down yet."
)

FlightHasArrivedFrame = NlgTemplate(
    "Yes, that got in {datetime}.",
    "Yes it did, it landed {datetime}.",
    "Indeed it has, it got in {datetime}.",
    "Yes, it looks like it landed {datetime}.",
    "In fact, it has. It got in {datetime}."
)

AirportCityFrame = NlgTemplate(
    "So in {city}, there's {airport_list}.",
    "There's {airport_list} in {city}.",
    "As far as I know, there's {airport_list} in {city}."
)

NoAirportsFoundInCity = NlgTemplate(
    "You know, I actually couldn't find any airports in {city}.",
    "I don't think I know of any airports in {city}."
)

MultipleCitiesFoundFrame = NlgTemplate(
    "Which city do you mean, {city_disambig_list}?",
    "I'm not sure if you're talking about {city_disambig_list}."
)

HasntTakenOffFrame = NlgTemplate(
    "hasn't taken off from {origin} yet",
    "hasn't left {origin} yet",
    "hasn't gotten out of {origin} yet"
)

HasntArrivedFrame = NlgTemplate(
    "hasn't gotten in to {destination} yet",
    "has yet to arrive in {destination}",
    "hasn't landed in {destination} yet",
    "hasn't yet landed in {destination}"
)

TookOffAtFrame = NlgTemplate(
    "took off from {origin} {departure_pp}",
    "left {origin} {departure_pp}",
    "took off {departure_pp} from {origin}"
)

# perhaps instead of
# arrival_pp, arrival_p?
ArrivedAtFrame = NlgTemplate(
    "got into {destination} {arrival_pp}",
    "arrived in {destination} {arrival_pp}",
    "landed in {destination} {arrival_pp}"
)

TimeSpecPp = NlgTemplate(
    "around {datetime}",
    "at {datetime}"
)

FlightActionPuntFrame = NlgTemplate(
    "I'm not sure what it is you want me to tell you about {flight}.",
    "I don't quite understand what you want me to tell you about {flight}.",
    "I don't quite understand what you're asking about {flight}"
)

FoundFlightNotScheduleFrame = NlgTemplate(
    "So, I couldn't find {airline} flight {flightno} in the "
    "airline's schedule, but it {status_report}.",
    "For whatever reason I couldn't find {airline} {flightno} "
    "in the airline's schedule, but it {status_report}."
)

InFactLateFrame = NlgTemplate(
    "Yes, it {was_or_is_running} late sad to say.",
    "Yes, it {was_or_is_running} a bit on the late end.",
)

InFactEarlyFrame = NlgTemplate(
    "Yes, it looks like it {is_or_going_to_be} a bit early.",
    "Yes, it seems like it {is_or_going_to_be} somewhat early, actually."
)

InFactOnTime = NlgTemplate(
    "Yes, it seems like it's {running_or_arrived} right on schedule.",
    "Yes, it's {running_or_arrived} on time."
)

NotActually = NlgTemplate(
    "It looks like it's actually {running_or_arrived} {a_bit_or_not} {early_late_or_on_time}.",
    "Actually, it's {running_or_arrived} {a_bit_or_not} {early_late_or_on_time}."
)

ArrivedOrDepartedAtFrame = NlgTemplate(
    "{nsubj} {has_or_hasnt_arrived_or_departed}.",
    "It looks like {nsubj} {has_or_hasnt_arrived_or_departed}.",
    "So, it looks like {nsubj} {has_or_hasnt_arrived_or_departed}.",
    "So, {nsubj} {has_or_hasnt_arrived_or_departed}."

)

FlightScheduleFrame = NlgTemplate(  # when user_scheduling_focus parameter is not picked up
    # reply_in_kind should be "So,"
    "{reply_in_kind} It looks like {nsubj} left {origin} around"
    " {time_diff_dep} {and_or_but} {has_or_hasnt_arrived}",
    "{reply_in_kind} {nsubj} took off from {origin} "
    "{time_diff_dep} {and_or_but} {has_or_hasnt_arrived}"
)

AirlineFlightFrame = NlgTemplate(
    "{airline} flight {flightno}",
    "{airline} {flightno}",
    "flight {flightno} on {airline}"
)

DeparturePhrase = NlgTemplate(
    "taken off",
    "left",
    "departed"
)
ArrivalPhrase = NlgTemplate(
    "landed",
    "gotten in",
    "arrived"
)


class NlgServer:
    MONTH_MAP = {
        1: "January",
        2: "February",
        3: "March",
        4: "April",
        5: "May",
        6: "June",
        7: "July",
        8: "August",
        9: "September",
        10: "October",
        11: "November",
        12: "December"
    }

    CHCKPT_MAP = {
        'departure': DeparturePhrase,
        'arrival': ArrivalPhrase,
        
    }

    def _realizeCheckpointPhrase(self, checkpoint, checkpoint_time, point):
        '''checkpoint is user action, either arrival or departure,
        checkpoint_time is the time of arrival or departure, 
        point refers to origin or destination, depending on action'''
        if checkpoint_time:
            if checkpoint == 'arrival':
                out = ArrivedAtFrame._realize(params={
                    'destination': point,
                    'arrival_pp': TimeSpecPp._realize(
                        params={'datetime': self._realizeTime(checkpoint_time, pp=False)})
                })
            else:
                out = TookOffAtFrame._realize(params={
                    'origin': point,
                    'departure_pp': TimeSpecPp._realize(
                        params={'datetime': self._realizeTime(checkpoint_time, pp=False)})
                })
        else:
            if checkpoint == 'arrival':
                out = HasntArrivedFrame._realize(
                    params={'destination': point})
            else:
                out = HasntTakenOffFrame._realize(
                    params={'origin': point})
        return out

    def FlightTimeDepOrArrResponse(self, action, airline, flightno, origin,
                                   destination, departure_time, arrival_time, elide=False):
        if action == 'arrival':
            city = destination
            check_time = arrival_time
        else:
            city = origin
            check_time = departure_time
        has_or_hasnt_arrived_or_departed = self._realizeCheckpointPhrase(
            action, check_time, city)
        if elide:
            nsubj = "It"
        else:
            nsubj = AirlineFlightFrame._realize(params={'airline': airline,
                                                        'flightno': flightno})
        return ArrivedOrDepartedAtFrame._realize(
            params={
                'nsubj': nsubj,
                'has_or_hasnt_arrived_or_departed': has_or_hasnt_arrived_or_departed
            })

    def FoundFlightNotScheduleResponse(self, airline, flightno, origin,
                                       destination, departure_time, arrival_time):
        if not departure_time:
            return HasntTakenOffFrame._realize(
                params={'origin': origin})
        # departure
        departure_pp = self._realizeTime(departure_time)
        departure_pred = TookOffAtFrame._realize(
            params={'origin': self._cityFromICAO(origin),
                    'departure_pp': departure_pp})
        # arrival
        if arrival_time:
            arrival_pp = self._realizeTime(arrival_time)
            arrival_pred = ArrivedAtFrame._realize(
                params={'destination': self._cityFromICAO(destination),
                        'arrival_pp': arrival_pp})
        else:
            arrival_pred = HasntArrivedFrame._realize(
                params={'origin': origin})
        status_report = departure_pred + " and " + arrival_pred
        return FoundFlightNotScheduleFrame._realize(
            params={'status_report': status_report, 'airline': airline, 'flightno': flightno})

    def ProvideExampleFlightResponse(self, airline, flightno):
        return Ok._realize() + ExampleFlightFrame._realize(
            params={'airline': airline,
                    'flightno': flightno})

    def FlightActionNotUnderstood(airline, flightno):
        flight_phrase = AirlineFlightFrame._realize(params={
                'airline': airline,
                'flightno': flightno
            })
        response = FlightActionPuntFrame._realize(params={
                'flight': flight_phrase
            })
        return response

    def _cityFromICAO(self, icao):
        try:
            return AIRPORT_DATA[icao]["city"]
        except KeyError:
            return icao

    def _scheduleAndOrBut(self, dep, arr):
        '''realize 'and' if both arrival and departure
        are either late or early, and 'but' if
        one is late and the other is early'''
        if arr is None:
            return 'and'
        else:
            return 'and' if (dep > 0) == (arr > 0) else 'but'

    def _realizeScheduleDiff(self, dval, WINDOW_IN_SECONDS=600):
        # give a 10 minute window for early/lateness
        if dval > WINDOW_IN_SECONDS:
            out = ' late'
        elif dval <= -WINDOW_IN_SECONDS:
            out = ' early'
        else:
            out = "on time"
            return out
        # now unpack timediff in second
        # to hrs, minutes.
        dval = abs(dval)
        hours, r = divmod(dval, 3600)
        minutes, r = divmod(r, 60)
        hr_plural_or_not = 's' if hours != 1 else ''
        mn_plural_or_not = 's' if minutes != 1 else ''
        text_hours = "{} hour{}".format(
            hours, hr_plural_or_not) if hours else ''
        text_minutes = "{} minute{}".format(
            minutes, mn_plural_or_not) if minutes else ''
        conj = " and " if text_hours else ''
        out = text_hours + conj + text_minutes + out
        return out

    def _realizeTime(self, timestamp, pp=True):
        dt = datetime.fromtimestamp(timestamp)
        if dt.hour % 12 == 0:
            m = "AM"
            if dt.hour == 0:
                hour = 12
            else:
                hour = dt.hour
        else:
            m = "PM"
            hour = dt.hour % 12
        time_phrase = "{}:{:>02d} {}".format(hour, dt.minute, m)
        now = datetime.now()
        if dt.day == now.day:
            time_phrase = "at " + time_phrase if pp else time_phrase
        elif dt.day == (now - timedelta(1)).day:
            time_phrase = "yesterday " + "at " + out
        else:
            time_phrase = "{} {}{} at ".format(self.MONTH_MAP[dt.month],
                                               dt.day,
                                               ordsuffix(dt.day)) + time_phrase
            time_pharse = "on " + time_phrase if pp else time_phrase
        return time_phrase

    def SmartPunt(self, base_frame, flight_number=None,
                  airline=None, origin=None,
                  destination=None, departure_time=None):
        output = base_frame
        if flight_number:
            output += str(flight_number) + " "
        if airline:
            output += self._realize(AirlinePp, params={'airline': airline})
        # TODO: recognize today and tomorrow
        if origin:
            output += self._realize(OriginPp, params={'origin': origin})
        if destination:
            output += self._realize(DestinationPp,
                                    params={'destination': destination})
        if departure_time:
            odt = datetime.fromtimestamp(int(departure_time))
            month = MONTH_MAP[odt.month]
            day = odt.day
            output += self._realize(DepartureDateTimePp,
                                    ["{} {}".format(month, day)])
            # Responding with the exact time sounds a bit stilted
            output += "at that time."
        else:
            output += "."
        return output

    def _airlineNameFromICAO(self, icao):
        keys = [k for k in AIRLINE_CODES if AIRLINE_CODES[k] == icao]
        # capitalize
        keys = [" ".join(w.capitalize() for w in k.split(" ")) for k in keys]
        return random.choice(keys)

    def FlightNotFoundSmartPunt(self, flight_number=None, airline=None,
                                origin=None, destination=None, departure_time=None):
        output = NoFlightsGivenParams._realize()
        if flight_number:
            output += str(flight_number) + " "
        if airline:
            output += AirlinePp._realize(
                params={'airline': airline})
        # TODO: recognize today and tomorrow
        if origin:
            output += OriginPp._realize(params={'origin': origin})
        if destination:
            output += DestinationPp._realize(
                params={'destination': destination})
        if departure_time:
            odt = datetime.fromtimestamp(int(departure_time))
            month = self.MONTH_MAP[odt.month]
            day = odt.day
            output += DepartureDateTimePp._realize(
                params={'departure_time': "{} {}".format(month, day)})
            # Responding with the exact time sounds a bit stilted
            output += "at that time."
        else:
            output = output.strip()
            output += "."
        return output

    def MultipleFlightsFoundSmartPunt(self, num_found, flight_number=None, airline=None, origin=None, destination=None, departure_time=None):
        frame = self._realize(self.NoFlightsGivenParams)
        return SmartPunt(base_frame=frame,
                         flight_number=flight_number,
                         airline=airline,
                         origin=origin,
                         destination=destination,
                         departure_time=departure_time)

    def _realizeArrival(self, arr_delay, destination):
        if arr_delay is None:
            return HasNotArrived._realize(params={'destination': destination})
        else:
            return HasArrived._realize(
                params={'time_diff_arr': self._realizeScheduleDiff(arr_delay),
                        'destination': destination})

    def _realizeCheckPointScheduleDiff(self, delay, chkpt, poi):
        if delay is None:
            if chkpt == 'arrival':
                response = HasNotArrived._realize(params={'destination': poi})
            else:
                response = HasntTakenOffFrame._realize(params={'origin': poi})
        else:
            if chkpt == 'arrival':
                response = HasArrived._realize(
                    params={'time_diff_arr': self._realizeScheduleDiff(delay),
                            'destination': poi})
            else:
                response = TookOffAtFrame._realize(
                    params={'origin': poi,
                            'departure_pp': self._realizeScheduleDiff(delay)})
        return response

    def _realizeReplyInKind(self, user_focus, dep_diff, arr_diff, dep_act, arr_act, chckpt):
        # User focus is 'early,' 'late,' or 'on time'
        if user_focus:
            # check if the flight checkpoint (either takeoff or landing)
            # has actually happened yet
            chkpt_cplt = bool(arr_act != 0) \
                if chckpt == 'arrival' else bool(dep_act != 0)
            # Determine whether the user is looking for
            # a departure or arrival, and whether that has happened yet.
            # If it hasn't, we just say 'running.'
            try:
                running_or_checkpoint = self.CHCKPT_MAP[chckpt]._realize(
                ) if chkpt_cplt else 'running'
            # KeyError occurs when classifier fails to pick up on
            # whether the user is talking about takeoff or landing.
            # But we don't want the reply in kind to cause the whole thing
            # to fail. 
            # The overall response falls back to talking about both in this case
            except KeyError:
                return ""

            if arr_diff is not None and dep_diff is not None:  # indicates the arrival happened
                early = bool(arr_diff < -600)
                late = bool(arr_diff > 600)
                on_time = bool(not early and not late)
                # AdvP attached to either "arrived" of "departed"
                act_arrived = 'on time' if on_time else 'early' if early else 'late'
            else:
                return ""
            if act_arrived != user_focus:
                na_params = {
                    'running_or_arrived': running_or_checkpoint,
                    'early_late_or_on_time': act_arrived,
                    'a_bit_or_not': 'a bit' if act_arrived != 'on time' else ''
                }
                resp = NotActually._realize(params=na_params)
            else:
                if user_focus == 'on time':
                    # check if flight arrived on time
                    resp = InFactOnTime._realize(
                        params={'running_or_arrived': running_or_checkpoint})
                elif user_focus == 'early':
                    resp = InFactEarlyFrame._realize(params={
                        'is_or_going_to_be': 'is' if chkpt_cplt else 'going to be'
                    })
                else:
                    resp = InFactLateFrame._realize(
                        params={
                            'was_or_is_running': 'was' if chkpt_cplt else 'is running'
                        })
        else:
            resp = ""
        return resp

    def FlightScheduleResponse(self, flightStruct, airline, flightno,
                               chckpt='', user_schedule_focus='', elide=False):
        '''NLG function for when the user asks if a flight will be early, late or on time'''
        dep_schdiff = int(flightStruct['departure'])
        arr_schdiff = int(
            flightStruct['arrival']) if flightStruct['arrival'] is not None else None
        origin_city = self._cityFromICAO(flightStruct['origin'])
        destination_city = self._cityFromICAO(flightStruct['destination'])
        time_diff_dep = self._realizeScheduleDiff(dep_schdiff)
        conj = self._scheduleAndOrBut(dep_schdiff, arr_schdiff)
        reply_in_kind = self._realizeReplyInKind(user_schedule_focus,
                                                 flightStruct['departure'],
                                                 flightStruct['arrival'],
                                                 flightStruct['departure_act'],
                                                 flightStruct['arrival_act'],
                                                 chckpt)
        if elide:
            nsubj = "it"
        else:
            nsubj = AirlineFlightFrame._realize(
                params={'airline': airline, 'flightno': int(flightno)})
        # If the user specifically refers to either takeoff or landing,
        # respond specifically with a phrase referring to that
        if chckpt:
            if chckpt == "arrival":
                poi = destination_city
                cp_time = arr_schdiff
            else:
                poi = origin_city
                cp_time = dep_schdiff
            has_or_hasnt_arrived_or_departed = self._realizeCheckPointScheduleDiff(
                cp_time,
                chckpt,
                poi
            )
            response = reply_in_kind + " " + ArrivedOrDepartedAtFrame._realize(
                params={
                    'nsubj': nsubj,
                    'has_or_hasnt_arrived_or_departed': has_or_hasnt_arrived_or_departed
                })
        else:
            schedule_desc = self._realizeCheckPointScheduleDiff(arr_schdiff,
                                                                'arrival',
                                                                destination_city)
            response = FlightScheduleFrame._realize(
                params={
                    'nsubj': nsubj,
                    'origin': origin_city,
                    'destination': destination_city,
                    'time_diff_dep': time_diff_dep,
                    'has_or_hasnt_arrived': schedule_desc,
                    'and_or_but': conj,
                    'reply_in_kind': reply_in_kind
                }).strip().split(" ")
            # Since flights that haven't yet arrived
            # may render reply_in_kind as empty string,
            # We want to make sure "it" is capitalized when it's
            # 1st in the case of elision
            response = " ".join([response[0].capitalize()] + response[1:])
        return response

    def HasFlightArrivedResponse(self, flightStruct):
        arr = int(flightStruct['arrivalTime'])
        if arr == 0:
            return FlightHasNotArrivedFrame._realize()
        else:
            return FlightHasArrivedFrame._realize(
                params={'datetime': self._realizeTime(arr)}).strip()

    def _realizeMissingScheduleInfo(self, params):
        '''An airline and flight number is required
        to search for flights, this will realize 
        a text segment representing the missing data'''
        missing_info = []
        if not params['airline']:
            missing_info.append('the airline')
        if not params['flightno']:
            missing_info.append('the flight number')
        if len(missing_info) == 2:
            random.shuffle(missing_info)
            return '{} and {}'.format(*missing_info)
        else:
            return missing_info[0]

    def GenerateMoreInfoPrompt(self, params):
        missing_info = self._realizeMissingScheduleInfo(params)
        return MoreInfoPrompt._realize(params={'missing_info': missing_info})

    def _realizeAirportInCity(self, ap_struct):
        return "{}, or {}".format(ap_struct['name'], ap_struct['icao'])

    def CityNotFoundResponse(self, city):
        return NoAirportsFoundInCity._realize(
            params={'city': city})

    def CityAirportList(self, city, airport_list):
        return AirportCityFrame._realize(
            params={'city': city,
                    'airport_list': nl_list([self._realizeAirportInCity(a)
                                             for a in airport_list])})

    def _realizeCityDisambigList(self, city_list, text_frame="{}, {}"):
        return nl_list([text_frame.format(c[0], c[1])
                        if c[1] else text_frame.format(c[0], c[2])
                        for c in city_list], conjunction="or")

    def MultipleCitiesFound(self, city_list):
        return MultipleCitiesFoundFrame._realize(
            params={'city_disambig_list': self._realizeCityDisambigList(city_list)})

    def UnrecognizedAirlinePunt(self, airline):
        return UnrecognizedAirline._realize(params={'airline': airline})

    def Punt(self):
        return GenericPunt._realize()
